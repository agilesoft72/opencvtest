﻿// dnnFace.cpp : 이 파일에는 'main' 함수가 포함됩니다. 거기서 프로그램 실행이 시작되고 종료됩니다.
// GoogLeNet model
//
#ifdef _DEBUG
#pragma comment(lib, "opencv_world410d.lib")
#else
#pragma comment(lib, "opencv_world410.lib")
#endif

#include "pch.h"
#include <iostream>

#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/videoio.hpp>


using namespace cv;
using namespace cv::dnn;
using namespace std;

const String model_face = "res10_300x300_ssd_iter_140000_fp16.caffemodel";
const String config_face = "res10_300x300_ssd_iter_140000_fp16.prototxt";

const String model_people = "MobileNetSSD_deploy.caffemodel";
const String config_people = "MobileNetSSD_deploy.prototxt";

VideoCapture cap(0);
VideoCapture capture;
Net net;

void doDetectFace(void);
void doDetectPeople(void);

int main(void)
{
	//net = readNet(model, config);
	net = readNet(model_people, config_people);

	if (net.empty()) {
		cerr << "Net open failed!" << endl;
		return -1;
	}

	//doDetectFace();
	doDetectPeople();



	return 0;
}

void doDetectPeople(void)
{
	Mat frame, frame_src;
	int delay,w,h;
	TickMeter tm;
	string filename = "C:\\Project_c\\OpenCV3.X.Test\\BackgroundSubtraction\\movie\\A_3_15.44.00_2X.avi";      // top view
	//string filename = "C:\\Project_c\\OpenCV3.X.Test\\BackgroundSubtraction\\movie\\A_3_15.44.00.avi";			// TOP VIEW
	//string filename = "C:\\Project_c\\OpenCV3.X.Test\\BackgroundSubtraction\\movie\\B_3_15.44.00.avi";
	//string filename = "C:\\Project_c\\OpenCV3.X.Test\\BackgroundSubtraction\\movie\\C_3_15.44.00.avi";

	capture = VideoCapture(filename);
	//capture = VideoCapture("c:\\Project_c\\OpenCV3.X.Test\\BackgroundSubtraction\\dnnFace\\videos\\example_01.mp4");
	if (!capture.isOpened()) {
		cerr << "동영상 open failed!" << endl;
		return;
	}

	double fps = capture.get(CAP_PROP_FPS);
	w = capture.get(CAP_PROP_FRAME_WIDTH);
	h = capture.get(CAP_PROP_FRAME_HEIGHT);

	int newW, newH;
	newW = 300;
	newH = 300;

	double startPos = 1500;
	capture.set(CAP_PROP_POS_FRAMES, startPos);

	
	// ************** 기록용 Video 설정. ( http://www.fourcc.org/codecs.php )
	int fourcc = VideoWriter::fourcc('F', 'M', 'P', '4');
	//int fourcc = VideoWriter::fourcc('D', 'I', 'V', 'X');
	//VideoWriter out("out.avi", fourcc, fps, Size(w, h));

	cout << "*** FPS :" << fps << endl << w << " X " << h;
	delay = cvRound(1000 / fps);

	while (capture.isOpened())
	{
		capture >> frame_src;
		if (frame_src.empty())
			break;

		resize(frame_src, frame, Size(newW, newH));

		Mat blob = blobFromImage(frame, 0.007843, Size(newW, newH), Scalar(104, 177, 123));
		net.setInput(blob);
		Mat res = net.forward();

		tm.reset();
		tm.start();
		Mat detect(res.size[2], res.size[3], CV_32FC1, res.ptr<float>());
		tm.stop();
		double ms = tm.getTimeMilli();
		
		cout << "detect time(msec) : " << ms << endl;

		for (int i = 0; i < detect.rows; i++) {
			float confidence = detect.at<float>(i, 2);
			if (confidence < 0.5)
				break;

			int x1 = cvRound(detect.at<float>(i, 3) * frame.cols);
			int y1 = cvRound(detect.at<float>(i, 4) * frame.rows);
			int x2 = cvRound(detect.at<float>(i, 5) * frame.cols);
			int y2 = cvRound(detect.at<float>(i, 6) * frame.rows);

			rectangle(frame, Rect(Point(x1, y1), Point(x2, y2)), Scalar(0, 255, 0));

			String label = format("(%d): %2.1f", i+1, confidence);
			putText(frame, label, Point(x1, y2 + 5), FONT_HERSHEY_SIMPLEX, 0.6, Scalar(0, 255, 0));
		}

		// 비디오 기록
		//out << frame;

		imshow("frame", frame);
		if (waitKey(1) == 27)
			break;
	}

	capture.release();
	//out.release();

	
}

void doDetectFace(void)
{
	if (!cap.isOpened()) {
		cerr << "Camera open failed!" << endl;
		return;
	}


	Mat frame;
	while (true) {
		cap >> frame;
		if (frame.empty())
			break;

		Mat blob = blobFromImage(frame, 1, Size(300, 300), Scalar(104, 177, 123));
		net.setInput(blob);
		Mat res = net.forward();

		Mat detect(res.size[2], res.size[3], CV_32FC1, res.ptr<float>());

		for (int i = 0; i < detect.rows; i++) {
			float confidence = detect.at<float>(i, 2);
			if (confidence < 0.5)
				break;

			int x1 = cvRound(detect.at<float>(i, 3) * frame.cols);
			int y1 = cvRound(detect.at<float>(i, 4) * frame.rows);
			int x2 = cvRound(detect.at<float>(i, 5) * frame.cols);
			int y2 = cvRound(detect.at<float>(i, 6) * frame.rows);

			rectangle(frame, Rect(Point(x1, y1), Point(x2, y2)), Scalar(0, 255, 0));

			String label = format("Face: %4.3f", confidence);
			putText(frame, label, Point(x1, y1 - 1), FONT_HERSHEY_SIMPLEX, 0.8, Scalar(0, 255, 0));
		}

		imshow("frame", frame);
		if (waitKey(1) == 27)
			break;
	}

}

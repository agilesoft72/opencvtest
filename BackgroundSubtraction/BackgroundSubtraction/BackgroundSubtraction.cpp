﻿// Moving Average Backgorund Subtraction
// OpenCV 3.4.6
//

#include "pch.h"
#include <iostream>
#include <opencv2/opencv.hpp>

#ifdef _DEBUG
#pragma comment(lib, "opencv_world346d.lib")
#else
#pragma comment(lib, "opencv_world346.lib")
#endif


using namespace cv;
using namespace std;

void draw_rect(Mat img, vector<Rect> &v_rect) {
	for (auto it : v_rect) {
		rectangle(img, it, CV_RGB(255, 0, 0), 2);
	}
}

int main()
{
	std::cout << "Hello World!\n";

	VideoCapture stream1(0);

	if (!stream1.isOpened()) {
		cout << "카메라를 열수 없습니다.";
	}

	Mat frame;
	Mat frame_img_gray;
	Mat sub_frame;
	Mat background_img_gray;
	int frame_flow = 50;
	int count_frame = 0;
	float min_area_percent = 0.1;
	float min_window_area;
	float alpha = 0.1;
	float beta = (1.0 - alpha);

	namedWindow("frame", 0);
	namedWindow("sub_frame", 0);
	namedWindow("BG_frame", 0);

	while (1)
	{
		if (!stream1.read(frame))
			break;

		// frame flow
		if (background_img_gray.empty()) {
			cvtColor(frame.clone(), background_img_gray, COLOR_BGR2GRAY);
			min_window_area = background_img_gray.size().area() * min_area_percent / 100.0;

			cout << "size: " << background_img_gray.size() << " area: " << background_img_gray.size().area() << " min_window_area: " << min_window_area << endl;
		}

		cvtColor(frame.clone(), frame_img_gray, COLOR_BGR2GRAY);
		addWeighted(frame_img_gray, alpha, background_img_gray, beta, 0.0, background_img_gray);

		if (count_frame < frame_flow)
		{
			count_frame++;
			printf("calibration %d\n", count_frame);
			continue;
		}


		//subtract(old_frame, frame, sub_frame);
		absdiff(background_img_gray, frame_img_gray, sub_frame);
		threshold(sub_frame, sub_frame, 70, 255, THRESH_BINARY);

		//find contour
		vector< vector< Point> > contours;
		vector< Vec4i> hierarchy;
		findContours(sub_frame.clone(), contours, hierarchy, RETR_CCOMP, CHAIN_APPROX_SIMPLE);
		//drawContours(frame, contours, -1, CV_RGB(255, 0, 0), 5, 8, hierarchy);

		//Blob labeling
		vector< Rect > v_rect;
		for (auto it : contours) {

			double area = contourArea(it, false);
			if (area > min_window_area)
			{
				Rect mr = boundingRect(Mat(it));
				v_rect.push_back(mr);
				//printf("%lf\n", area);
			}
		}
		draw_rect(frame, v_rect);

		//
		imshow("frame", frame);
		imshow("sub_frame", sub_frame);
		imshow("BG_frame", background_img_gray);


		if (waitKey(5) >= 0)
			break;
	}

	return 0;

}

// 프로그램 실행: <Ctrl+F5> 또는 [디버그] > [디버깅하지 않고 시작] 메뉴
// 프로그램 디버그: <F5> 키 또는 [디버그] > [디버깅 시작] 메뉴

// 시작을 위한 팁: 
//   1. [솔루션 탐색기] 창을 사용하여 파일을 추가/관리합니다.
//   2. [팀 탐색기] 창을 사용하여 소스 제어에 연결합니다.
//   3. [출력] 창을 사용하여 빌드 출력 및 기타 메시지를 확인합니다.
//   4. [오류 목록] 창을 사용하여 오류를 봅니다.
//   5. [프로젝트] > [새 항목 추가]로 이동하여 새 코드 파일을 만들거나, [프로젝트] > [기존 항목 추가]로 이동하여 기존 코드 파일을 프로젝트에 추가합니다.
//   6. 나중에 이 프로젝트를 다시 열려면 [파일] > [열기] > [프로젝트]로 이동하고 .sln 파일을 선택합니다.

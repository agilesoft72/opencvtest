﻿// HAAR.cpp : 이 파일에는 'main' 함수가 포함됩니다. 거기서 프로그램 실행이 시작되고 종료됩니다.
//

#include "pch.h"
#include <opencv2/objdetect.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/videoio.hpp>
#include <iostream>
#include <iomanip>

#ifdef _DEBUG
#pragma comment(lib, "opencv_world410d.lib")
#else
#pragma comment(lib, "opencv_world410.lib")
#endif


using namespace cv;
using namespace std;

void detectFace(Mat frame);

CascadeClassifier face_cascade;

int main()
{
	String face_cascade_name;
	String filename;
	
	
	//face_cascade_name = "c:\\OpenCV.4.1.0\\opencv\\build\\etc\\haarcascades\\haarcascade_frontalface_alt2.xml";
	//face_cascade_name = "c:\\OpenCV.4.1.0\\opencv\\build\\etc\\haarcascades\\haarcascade_upperbody.xml";
	//face_cascade_name = "c:\\OpenCV.4.1.0\\opencv\\build\\etc\\haarcascades\\haarcascade_lowerbody.xml";
	face_cascade_name = "c:\\OpenCV.4.1.0\\opencv\\build\\etc\\haarcascades\\haarcascade_fullbody.xml";
	//face_cascade_name = "c:\\OpenCV.4.1.0\\opencv\\build\\etc\\haarcascades\\haarcascade_upperbody.xml";
	//face_cascade_name = "c:\\OpenCV.4.1.0\\opencv\\build\\etc\\haarcascades\\haarcascade_lowerbody.xml";
	// *************************************************************************************
	// LBP Classifier
	// *************************************************************************************
	//face_cascade_name = "c:\\OpenCV.3.4.6\\opencv\\build\\X32\\etc\\lbpcascades\\lbpcascade_frontalcatface.xml";	
	//face_cascade_name = "c:\\OpenCV.3.4.6\\opencv\\build\\X32\\etc\\lbpcascades\\lbpcascade_frontalface_improved.xml";
	//face_cascade_name = "c:\\OpenCV.3.4.6\\opencv\\build\\X32\\etc\\lbpcascades\\lbpcascade_profileface.xml";
	//face_cascade_name = "c:\\OpenCV.3.4.6\\opencv\\build\\X32\\etc\\lbpcascades\\lbpcascade_silverware.xml";
	//face_cascade_name = "c:\\OpenCV.3.4.6\\opencv\\build\\X32\\etc\\lbpcascades\\lbpcascade_frontalface.xml";


	filename = "c:\\Project_c\\samples\\ele13.jpg";


	namedWindow("image", WINDOW_AUTOSIZE);

	Mat img = imread(filename, IMREAD_COLOR);

	if (!img.data) {
		cout << "이미지 읽기 오류.\n";
		getchar();
		return -1;
	}
	
	if (!face_cascade.load(face_cascade_name)) {
		cout << "xml 로딩 오류\n";
		return 0;
	}

	detectFace(img);


	imshow("image", img);
	waitKey(0);
	return 0;
}

void detectFace(Mat frame)
{
	vector<Rect> faces;
	Mat gray_frame;
	TickMeter tm;

	cvtColor(frame, gray_frame, COLOR_BGR2GRAY);
	equalizeHist(gray_frame, gray_frame);

	tm.start();
	face_cascade.detectMultiScale(gray_frame, faces, 1.1, 2, 0 | CASCADE_SCALE_IMAGE, Size(30, 30));
	tm.stop();
	double ms = tm.getTimeMilli();
	
	cout << "소요시간 :" << ms << " msec";

	for (size_t i = 0; i < faces.size(); i++)
	{
		Rect r = faces[i];
		rectangle(frame, r.tl(), r.br(), CV_RGB(0, 255, 0), 3);
	}
}

// 프로그램 실행: <Ctrl+F5> 또는 [디버그] > [디버깅하지 않고 시작] 메뉴
// 프로그램 디버그: <F5> 키 또는 [디버그] > [디버깅 시작] 메뉴
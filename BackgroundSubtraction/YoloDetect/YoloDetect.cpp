﻿// yolol & opencv 3.4.0 & CUDA 테스트.
//




#include "pch.h"
#include <opencv2/opencv.hpp>            // C++
#include <opencv2/core/version.hpp>
#ifndef CV_VERSION_EPOCH     // OpenCV 3.x and 4.x
#include <opencv2/videoio/videoio.hpp>
#define OPENCV_VERSION CVAUX_STR(CV_VERSION_MAJOR)"" CVAUX_STR(CV_VERSION_MINOR)"" CVAUX_STR(CV_VERSION_REVISION)
#ifndef USE_CMAKE_LIBS
#pragma comment(lib, "opencv_world" OPENCV_VERSION ".lib")
#ifdef TRACK_OPTFLOW
#pragma comment(lib, "opencv_cudaoptflow" OPENCV_VERSION ".lib")
#pragma comment(lib, "opencv_cudaimgproc" OPENCV_VERSION ".lib")
#pragma comment(lib, "opencv_core" OPENCV_VERSION ".lib")
#pragma comment(lib, "opencv_imgproc" OPENCV_VERSION ".lib")
#pragma comment(lib, "opencv_highgui" OPENCV_VERSION ".lib")
#endif    // TRACK_OPTFLOW
#endif    // USE_CMAKE_LIBS
#else     // OpenCV 2.x
#define OPENCV_VERSION CVAUX_STR(CV_VERSION_EPOCH)"" CVAUX_STR(CV_VERSION_MAJOR)"" CVAUX_STR(CV_VERSION_MINOR)
#ifndef USE_CMAKE_LIBS
#pragma comment(lib, "opencv_core" OPENCV_VERSION ".lib")
#pragma comment(lib, "opencv_imgproc" OPENCV_VERSION ".lib")
#pragma comment(lib, "opencv_highgui" OPENCV_VERSION ".lib")
#pragma comment(lib, "opencv_video" OPENCV_VERSION ".lib")
#endif    // USE_CMAKE_LIBS
#endif    // CV_VERSION_EPOCH

//#define RECORDING


#include "yolo_v2_class.hpp"  
#pragma comment(lib, "yolo_cpp_dll.lib")  

using namespace std;
using namespace cv;

using std::ostream;
using std::ofstream;
using std::cout;



std::vector<std::string> objects_names_from_file(std::string const filename) {
	std::ifstream file(filename);
	std::vector<std::string> file_lines;
	if (!file.is_open()) return file_lines;
	for (std::string line; getline(file, line);) file_lines.push_back(line);
	std::cout << "object names loaded \n";
	return file_lines;
}

void detectFromCam(Detector detector, vector<string> obj_names);
void detectFromFile(Detector detector, vector<string> obj_names);
void detectFromFile2(Detector detector, vector<string> obj_names);
void detectDiffer(Mat srcBack, Mat srcFrame, const char* s, double* r, int* cnt);
void on_trackbar(int pos, void* userdata);
int drawPerson(vector<bbox_t> result_vec, Mat frame, vector<string> obj_names);

double current_pos1=0;
double current_pos2=0;
double current_pos3=0;
TickMeter tm;
int newW;
int newH;


VideoCapture capFile1, capFile2, capFile3;
Mat back1, back2, back3;
Mat src1, src2, src3;

void draw_rect(Mat img, vector<Rect> &v_rect) {
	Rect r = v_rect[0];

	for (auto it : v_rect) {
		r = r | it;
		rectangle(img, it, CV_RGB(255, 0, 0), 2);
	}
	//rectangle(img, r, CV_RGB(255, 0, 0), 2);
	//cout << " diff rect : " << r << endl;
}


int main()
{
	std::string  names_file = "c:\\DARKNET\\install\\data\\coco.names";
	std::string  cfg_file = "c:\\DARKNET\\install\\cfg\\yolov3.cfg";
	std::string  weights_file = "c:\\DARKNET\\install\\weights\\yolov3.weights";
	std::string filename;

	auto obj_names = objects_names_from_file(names_file);
	Detector detector(cfg_file, weights_file);


	//detectFromCam(detector, obj_names);
	//detectFromFile(detector, obj_names);
	detectFromFile2(detector, obj_names);
}

void on_mouse(int event, int x, int y, int flags, void*)
{
	switch (event)
	{
	case EVENT_LBUTTONDBLCLK:
		current_pos1 = current_pos1 + (30 * 15);
		current_pos2 = current_pos1;
		current_pos3 = current_pos1;
		
		capFile1.set(CV_CAP_PROP_POS_FRAMES, current_pos1);
		capFile2.set(CV_CAP_PROP_POS_FRAMES, current_pos2);
		capFile3.set(CV_CAP_PROP_POS_FRAMES, current_pos3);

		cout << "move play position :" << current_pos1 << endl;
		break;
	default:
		break;
	}
}

void detectDiffer(Mat srcBack, Mat srcFrame, const char* s, double* r, int* cnt)
{
	Mat gray_frame1;
	Mat sub1;

	vector<Scalar> colors;
	colors.push_back(Scalar(0, 255, 0));
	colors.push_back(Scalar(255, 0, 204));
	colors.push_back(Scalar(255, 153, 0));
	colors.push_back(Scalar(0, 0, 255));

	cvtColor(srcFrame.clone(), gray_frame1, COLOR_BGR2GRAY);

	absdiff(back1, gray_frame1, sub1);
	threshold(sub1, sub1, 70, 255, THRESH_BINARY);

	Mat element5(15, 15, CV_8U, cv::Scalar(1));
	morphologyEx(sub1, sub1, MORPH_CLOSE, element5);

	vector< vector< Point> > contours;
	vector< Vec4i> hierarchy;
	findContours(sub1.clone(), contours, hierarchy, RETR_EXTERNAL, CHAIN_APPROX_NONE);

	double area = 0;
	int count = 0;
	int idx = 0;
	for (int i = 0; i < contours.size(); i++) {
		double segArea = contourArea(contours[i], false);
		area = area + segArea;
		Rect bRect = boundingRect(Mat(contours[i]));

		double rs = (100 * segArea) / (newW * newH);

		// 2% 미만 무시.
		if (rs < 2)
			continue;

		String label = format("A: %.1f", rs);
		Scalar color = colors[idx];
		idx++;
		if (idx >= colors.size())
			idx = 0;

		cv::putText(srcFrame, label, Point(bRect.x, bRect.y + bRect.height), FONT_HERSHEY_SIMPLEX, 0.6, Scalar(255, 255, 255));
		rectangle(srcFrame, bRect, color, 2);
		count++;
	}
	*r = (100 * area) / (newW * newH);
	*cnt = count;

	imshow(s, sub1);

}

void detectFromFile2(Detector detector, vector<string> obj_names)
{
	TickMeter tmLog;
	
	// 1920 X 1080

	string filename1 = "C:\\Project_c\\OpenCV3.X.Test\\BackgroundSubtraction\\movie\\CAM2.avi";      // top view
	string filename2 = "C:\\Project_c\\OpenCV3.X.Test\\BackgroundSubtraction\\movie\\CAM1.avi";
	string filename3 = "C:\\Project_c\\OpenCV3.X.Test\\BackgroundSubtraction\\movie\\CAM3.avi";
	//string filename1 = "C:\\Project_c\\OpenCV3.X.Test\\BackgroundSubtraction\\movie\\A_3_15.44.00_2X.avi";      // top view
	//string filename2 = "C:\\Project_c\\OpenCV3.X.Test\\BackgroundSubtraction\\movie\\B_3_15.44.00_2X.avi";
	//string filename3 = "C:\\Project_c\\OpenCV3.X.Test\\BackgroundSubtraction\\movie\\C_3_15.44.00_2X.avi";
	//string filename1 = "C:\\Project_c\\OpenCV3.X.Test\\BackgroundSubtraction\\movie\\A_3_15.44.00.avi";      // top view
	//string filename2 = "C:\\Project_c\\OpenCV3.X.Test\\BackgroundSubtraction\\movie\\B_3_15.44.00.avi";
	//string filename3 = "C:\\Project_c\\OpenCV3.X.Test\\BackgroundSubtraction\\movie\\C_3_15.44.00.avi";

	capFile1 = VideoCapture(filename1);
	capFile2 = VideoCapture(filename2);
	capFile3 = VideoCapture(filename3);
	if (!capFile1.isOpened()) {
		cerr << "동영상#1 open failed!" << endl;
		return;
	}
	if (!capFile2.isOpened()) {
		cerr << "동영상#2 open failed!" << endl;
		return;
	}
	if (!capFile3.isOpened()) {
		cerr << "동영상#3 open failed!" << endl;
		return;
	}

	double fps = capFile1.get(CAP_PROP_FPS);
	int w = capFile1.get(CAP_PROP_FRAME_WIDTH);
	int h = capFile1.get(CAP_PROP_FRAME_HEIGHT);

	newW = 416;
	newH = 416;
	//newW = w / 2;
	//newH = h / 2;

	Mat frame_src1, frame1;
	Mat frame_src2, frame2;
	Mat frame_src3, frame3;
	
	int frame_max1 = capFile1.get(CAP_PROP_FRAME_COUNT);

	cout << "FPS :" << fps << " " << w << " X " << h << "  크기조정:" << newW << " X " << newH << endl;

	cout << "Net Width: " << detector.get_net_width() << " Net Height: " << detector.get_net_height() << endl;

	namedWindow("frame1");
	moveWindow("frame1", 0, 0);
	namedWindow("frame2");
	moveWindow("frame2", newW, 0);
	namedWindow("frame3");
	moveWindow("frame3", 0, newH + 50);

	namedWindow("diff");
	moveWindow("diff", newW, newH + 50);
	namedWindow("result");
	moveWindow("result", newW*2, newH + 50);
	//namedWindow("back");
	//moveWindow("back", newW*3, newH + 50);

	setMouseCallback("frame1", on_mouse);


	// 첫 디텍트 시작 지점.
	//double startPos = 1;
	//capFile1.set(CV_CAP_PROP_POS_FRAMES, startPos);
	//capFile2.set(CV_CAP_PROP_POS_FRAMES, startPos);
	//capFile3.set(CV_CAP_PROP_POS_FRAMES, startPos);

	#ifdef RECORDING
	int fourcc = VideoWriter::fourcc('F', 'M', 'P', '4');
	VideoWriter out("out.avi", fourcc, fps, Size(newW, newH));
	#endif

	bool flag = false;
	int delay = cvRound(1000 / fps);

	tmLog.reset();
	tmLog.start();
		
	int picNo = 1;
	while (capFile1.isOpened())
	{
		if (!capFile1.grab()) {
			cout << "frame empty" << endl;
			break;
		}
		if (!capFile2.grab()) {
			cout << "frame empty" << endl;
			break;
		}
		if (!capFile3.grab()) {
			cout << "frame empty" << endl;
			break;
		}

		tm.reset();
		tm.start();

		float tresh = 0.1;
		// *************************************************************
		current_pos1 = capFile1.get(CV_CAP_PROP_POS_FRAMES);		
		capFile1.retrieve(frame_src1);
		resize(frame_src1, frame1, Size(newW, newH));		
		src1 = frame1.clone();
		Mat pic = frame1.clone();
		//vector<bbox_t> result_vec1 = detector.detect(frame1, tresh, false);
		//int detectCnt = drawPerson(result_vec1, frame1, obj_names);
		// *************************************************************
		current_pos2 = capFile2.get(CV_CAP_PROP_POS_FRAMES);		
		capFile2.retrieve(frame_src2);
		resize(frame_src2, frame2, Size(newW, newH));
		src2 = frame2.clone();
		//vector<bbox_t> result_vec2 = detector.detect(frame2, tresh, false);
		//drawPerson(result_vec2, frame2, obj_names);
		// *************************************************************
		current_pos3 = capFile3.get(CV_CAP_PROP_POS_FRAMES);		
		capFile3.retrieve(frame_src3);
		resize(frame_src3, frame3, Size(newW, newH));
		src3 = frame3.clone();
		//vector<bbox_t> result_vec3 = detector.detect(frame3, tresh, false);
		//drawPerson(result_vec3, frame3, obj_names);
		// *************************************************************

		if (back1.empty()) {
			cvtColor(frame1.clone(), back1, COLOR_BGR2GRAY);			
			cout << "background fetched." << endl;
			imshow("back", back1);
		}

		// ************************************************************
		double diffAreaRatio = 0;
		int    diffCount = 0;
		detectDiffer(back1, frame1, "diff", &diffAreaRatio, &diffCount);

		if (diffAreaRatio > 2) {
			tmLog.stop();
			if (tmLog.getTimeSec() > 1) {
				cout << "diff ratio: " << diffAreaRatio << " obj count : " << diffCount << " Pos: "<< current_pos1 << endl;
				tmLog.reset();			
				
				String savefile = format("%010d.jpg", picNo);
				imwrite(savefile, pic);
				picNo++;				
			}
			tmLog.start();			
		}
		
		// ************************************************************

		if (diffAreaRatio > 2.0) {
			String label;
			Scalar color;
			if ((diffCount >= 2) || (diffAreaRatio >= 12.0 && diffCount == 1) || (diffAreaRatio >= 13)) {
				label = "NO!";
				color = Scalar(0, 0, 255);
			}
			else {
				label = "OK!";
				color = Scalar(255, 255, 255);
			}

			int x = (src1.size().width / 2) - 50;
			int y = src1.size().height - 100;
			cv::putText(src1, label, Point(x, y), FONT_HERSHEY_SIMPLEX, 3, color);
		}

		imshow("result", src1);

		//if (diffArea < 2.0) {
		//	current_pos1 = current_pos1 + (30 * 5);
		//	current_pos2 = current_pos1;
		//	current_pos3 = current_pos1;

		//	capFile1.set(CV_CAP_PROP_POS_FRAMES, current_pos1);
		//	capFile2.set(CV_CAP_PROP_POS_FRAMES, current_pos2);
		//	capFile3.set(CV_CAP_PROP_POS_FRAMES, current_pos3);

		//	cout << "move play position :" << current_pos1 << endl;
		//}

		#ifdef RECORDING
		out << src1;
		#endif

		imshow("frame1", frame1);
		imshow("frame2", frame2);
		imshow("frame3", frame3);

		tm.stop();
		int newDelay = delay - tm.getTimeSec();

		if (waitKey(newDelay) >= 0)
			break;
	}
	#ifdef RECORDING
	out.release();
	#endif // RECORDING

	destroyAllWindows();
}

int drawPerson(vector<bbox_t> result_vec, Mat frame, vector<string> obj_names)
{
	int person_cnt = 0;

	vector<Scalar> colors;
	colors.push_back(Scalar(0, 255, 0));
	colors.push_back(Scalar(255, 0, 204));
	colors.push_back(Scalar(255, 153, 0));
	colors.push_back(Scalar(0, 0, 255));

	for (int i = 0; i < result_vec.size(); i++)
	{
		int x1 = result_vec[i].x;
		int y1 = result_vec[i].y;
		int x2 = x1 + result_vec[i].w;
		int y2 = y1 + result_vec[i].h;

		

		bool person = false;

		if (obj_names.size() > result_vec[i].obj_id)
		{
			string obj_name = obj_names[result_vec[i].obj_id];
			if (obj_name._Equal("person")) {
				rectangle(frame, Rect(Point(x1, y1), Point(x2, y2)), colors[i], 2);
				//rectangle(frame, Rect(Point(x1, y1), Point(x2, y2)), Scalar(0, 255, 0), 2);

				double area = (x2 - x1) * (y2 - y1);
				String label = format("prob: %f", result_vec[i].prob);
				int yPos = y1 + ((y2 - y1) / 2);
				cv::putText(frame, label, Point(x1, yPos ), FONT_HERSHEY_SIMPLEX, 0.6, Scalar(0, 255, 0));

				double areaR = (area * 100) / (newH * newH);
				label = format("Area: %.1f", areaR);
				yPos = yPos + 20;
				cv::putText(frame, label, Point(x1, yPos), FONT_HERSHEY_SIMPLEX, 0.6, Scalar(51, 204, 51));
				
				person_cnt++;
			}
		}
	}

	String label = format("Count: %d", person_cnt);
	putText(frame, label, Point(10, 20), FONT_HERSHEY_SIMPLEX, 0.6, Scalar(0, 255, 0));

	return person_cnt;
}

void on_trackbar(int pos, void* userdata)
{
	capFile1.set(CV_CAP_PROP_POS_FRAMES, pos);
	capFile2.set(CV_CAP_PROP_POS_FRAMES, pos);
	capFile3.set(CV_CAP_PROP_POS_FRAMES, pos);
	cout << "track bar : " << pos << endl;
}


void detectFromFile(Detector detector, vector<string> obj_names)
{
	VideoCapture cap;
	// 1920 X 1080

	string filename = "C:\\Project_c\\OpenCV3.X.Test\\BackgroundSubtraction\\movie\\A_3_15.44.00.avi";      // top view
	//string filename = "C:\\Project_c\\OpenCV3.X.Test\\BackgroundSubtraction\\movie\\B_3_15.44.00.avi";
	//string filename = "C:\\Project_c\\OpenCV3.X.Test\\BackgroundSubtraction\\movie\\C_3_15.44.00.avi";

	cap = VideoCapture(filename);
	if (!cap.isOpened()) {
		cerr << "동영상 open failed!" << endl;
		return;
	}

	double fps = cap.get(CAP_PROP_FPS);
	int w = cap.get(CAP_PROP_FRAME_WIDTH);
	int h = cap.get(CAP_PROP_FRAME_HEIGHT);

	int newW = w / 4;
	int newH = h / 4;
	Mat frame_src, frame;

	int slide_pos;

	int frame_max = cap.get(CAP_PROP_FRAME_COUNT);

	cout << "FPS :" << fps << " " << w << " X " << h << "  크기조정:" << newW << " X " << newH << endl;

	while (cap.isOpened())
	{
		cap >> frame_src;
		if (frame_src.empty()) {
			cout << "frame empty" << endl;
			break;
		}

		resize(frame_src, frame, Size(newW, newH));

		vector<bbox_t> result_vec = detector.detect(frame, 0.2, false);

		int person_cnt = 0;

		for (int i = 0; i < result_vec.size(); i++)
		{
			int x1 = result_vec[i].x;
			int y1 = result_vec[i].y;
			int x2 = x1 + result_vec[i].w;
			int y2 = y1 + result_vec[i].h;

			bool person = false;

			if (obj_names.size() > result_vec[i].obj_id)
			{
				string obj_name = obj_names[result_vec[i].obj_id];
				if (obj_name._Equal("person")) {
					rectangle(frame, Rect(Point(x1, y1), Point(x2, y2)), Scalar(0, 255, 0), 2);
					person_cnt++;
				}
			}
		}

		String label = format("(person: %d)", person_cnt);
		putText(frame, label, Point(10, 10), FONT_HERSHEY_SIMPLEX, 0.6, Scalar(0, 255, 0));


		imshow("frame", frame);

		if (waitKey(1) >= 0)
			break;
	}
}

void detectFromCam(Detector detector, vector<string> obj_names)
{
	VideoCapture cap;
	cap.open(0);

	if (!cap.isOpened()) {
		cout << "카메라 열기 오류." << endl;
		return;
	}

	while (true)
	{
		Mat frame;
		cap >> frame;

		if (frame.empty())
		{
			cout << " 공백 프레임." << endl;
			break;
		}

		vector<bbox_t> result_vec = detector.detect(frame, 0.2, false);

		int person_cnt = 0;

		for (int i = 0; i < result_vec.size(); i++)
		{
			int x1 = result_vec[i].x;
			int y1 = result_vec[i].y;
			int x2 = x1 + result_vec[i].w;
			int y2 = y1 + result_vec[i].h;

			bool person = false;

			if (obj_names.size() > result_vec[i].obj_id)
			{
				string obj_name = obj_names[result_vec[i].obj_id];
				if (obj_name._Equal("person")) {
					rectangle(frame, Rect(Point(x1, y1), Point(x2, y2)), Scalar(0, 255, 0), 2);
					person_cnt++;
				}
			}
		}

		String label = format("(person: %d)", person_cnt);
		putText(frame, label, Point(10, 10), FONT_HERSHEY_SIMPLEX, 0.6, Scalar(0, 255, 0));


		imshow("frame", frame);

		if (waitKey(30) >= 0)
			break;
	}
}


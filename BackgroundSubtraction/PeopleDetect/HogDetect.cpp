﻿// PeopleDetect.cpp : 이 파일에는 'main' 함수가 포함됩니다. 거기서 프로그램 실행이 시작되고 종료됩니다.
//

#include "pch.h"
#include <opencv2/objdetect.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/videoio.hpp>
#include <iostream>
#include <iomanip>

#ifdef _DEBUG
#pragma comment(lib, "opencv_world410d.lib")
#else
#pragma comment(lib, "opencv_world410.lib")
#endif


using namespace cv;
using namespace std;


VideoCapture capture;
VideoCapture cap(0);

void doDetectPeople(void);

int main()
{

	doDetectPeople();

}

void doDetectPeople(void)
{	
	int delay, w, h;
	TickMeter tm;
	
	
	namedWindow("frame", 0);

	capture = VideoCapture("c:\\Project_c\\OpenCV3.X.Test\\BackgroundSubtraction\\dnnFace\\videos\\example_01.mp4");
	if (!capture.isOpened()) {
		cerr << "동영상 open failed!" << endl;
		return;
	}

	HOGDescriptor hog;
	hog.setSVMDetector(HOGDescriptor::getDefaultPeopleDetector());
	cout << "cellSize: " << hog.cellSize << " nbins:" << hog.nbins << " blockStride: " << hog.blockStride << " winSize: " << hog.winSize;
	// cellSize: [8 x 8] nbins:9 blockStride: [8 x 8] winSize: [64 x 128]

	double fps = capture.get(CAP_PROP_FPS);
	w = capture.get(CAP_PROP_FRAME_WIDTH);
	h = capture.get(CAP_PROP_FRAME_HEIGHT);

	cout << "*** FPS :" << fps << endl << "Width : " << w << " Height : " << h << endl;
	delay = cvRound(1000 / fps);

	Mat frame;
	while (capture.isOpened())
	{
		capture >> frame;
		if (frame.empty()) {
			cout << "frame empty" << endl;
			break;
		}

		Mat frame_img_gray;
		cvtColor(frame.clone(), frame_img_gray, COLOR_BGR2GRAY);
		equalizeHist(frame_img_gray, frame_img_gray);

		vector<Rect> found;
		vector<double> weights;
		tm.reset();
		tm.start();
		hog.detectMultiScale(frame_img_gray, found, weights, 0.0, Size(8,8), Size(0,0), 1.03, 2, false);
		//hog.detectMultiScale(frame, found, 0, Size(4, 4), Size(0, 0), 1.05, 2, false);
		//hog.detectMultiScale(frame, found);
		tm.stop();
		double ms = tm.getTimeMilli();
		//cout << " msec : " << ms << " sec :" << ms / 1000 << endl;

		for (size_t i = 0; i < found.size(); i++) {
			Rect r = found[i];
			
			rectangle(frame, r.tl(), r.br(), CV_RGB(0, 255, 0), 3);

			if (weights.size() > 0)
			{
				double weight = weights[i];
				ostringstream buf;
				buf << 'W :' << weight;
				int y = 60 + (i * 20);
				putText(frame, buf.str(), Point(10, y), FONT_HERSHEY_PLAIN, 1.2, Scalar(0, 0, 255), 1, LINE_AA);

			}
		}

		if (found.size() > 0)
		{
			ostringstream buf;
			buf << "People Count: " << found.size() << " Time:" << ms << " msec";
			putText(frame, buf.str(), Point(10, 30), FONT_HERSHEY_PLAIN, 1.2, Scalar(0, 0, 255), 1, LINE_AA);
		}


		//int newDelay = delay - tm.getTimeSec();
		
		imshow("frame", frame);
		if (waitKey(1) == 27)
			break;
	}

	capture.release();
}


// 프로그램 실행: <Ctrl+F5> 또는 [디버그] > [디버깅하지 않고 시작] 메뉴
// 프로그램 디버그: <F5> 키 또는 [디버그] > [디버깅 시작] 메뉴

// 시작을 위한 팁: 
//   1. [솔루션 탐색기] 창을 사용하여 파일을 추가/관리합니다.
//   2. [팀 탐색기] 창을 사용하여 소스 제어에 연결합니다.
//   3. [출력] 창을 사용하여 빌드 출력 및 기타 메시지를 확인합니다.
//   4. [오류 목록] 창을 사용하여 오류를 봅니다.
//   5. [프로젝트] > [새 항목 추가]로 이동하여 새 코드 파일을 만들거나, [프로젝트] > [기존 항목 추가]로 이동하여 기존 코드 파일을 프로젝트에 추가합니다.
//   6. 나중에 이 프로젝트를 다시 열려면 [파일] > [열기] > [프로젝트]로 이동하고 .sln 파일을 선택합니다.
